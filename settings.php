<?php

/**
 * Admin settings for the Language Confidence question type.
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    global $CFG;

    // Default settings for audio.
    $settings->add(new admin_setting_heading(
        'audiooptionsheading',
        get_string('optionsforaudio', 'qtype_languageconfidence'),
        ''
    ));

    // Recording time limit.
    $settings->add(new admin_setting_configduration(
        'qtype_languageconfidence/timelimit',
        get_string('timelimit', 'qtype_languageconfidence'),
        get_string('timelimit_desc', 'qtype_languageconfidence'),
        600,
        60
    ));

    // Audio bitrate.
    $settings->add(new admin_setting_configtext(
        'qtype_languageconfidence/audiobitrate',
        get_string('audiobitrate', 'qtype_languageconfidence'),
        get_string('audiobitrate_desc', 'qtype_languageconfidence'),
        128000,
        PARAM_INT,
        8
    ));

    //other settings
    $settings->add(new admin_setting_heading('other_settings_heading', get_string('other_settings_heading', 'qtype_languageconfidence'), ''));
    $settings->add(new admin_setting_configtext('qtype_languageconfidence/api_url', get_string('api_url', 'qtype_languageconfidence'), '', ''));
    $settings->add(new admin_setting_configtext(
        'qtype_languageconfidence/daysolderaudiofiles',
        get_string('daysolderaudiofiles', 'qtype_languageconfidence'),
        get_string('daysolderaudiofiles_desc', 'qtype_languageconfidence'),
        15,
        PARAM_INT,
        4
    ));
}
