<?php
/**
 * Language Confidence question type backup handler
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Provides the information to backup Language Confidence questions
 *
 * @copyright 2021 Language Confidence
 */
class backup_qtype_languageconfidence_plugin extends backup_qtype_plugin {

    /**
     * Returns the qtype information to attach to question element
     */
    protected function define_question_plugin_structure() {

        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, '../../qtype', 'languageconfidence');

        // Create one standard named plugin element (the visible container).
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // Connect the visible container ASAP.
        $plugin->add_child($pluginwrapper);

        // Now create the qtype own structures.
        $languageconfidence = new backup_nested_element('languageconfidence', array('id'), array(
            'mediatype', 'speechphrase', 'timelimitinseconds', 'accent'));

        // Now the own qtype tree.
        $pluginwrapper->add_child($languageconfidence);

        // Set source to populate the data.
        $languageconfidence->set_source_table('qtype_langconfid_options',
            array('questionid' => backup::VAR_PARENTID));

        // Don't need to annotate ids nor files.

        return $plugin;
    }
}
