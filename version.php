<?php
/**
 * Language Confidence question type version information.
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2022082600;
$plugin->requires  = 2020061500;
$plugin->component = 'qtype_languageconfidence';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = '1.5 for Moodle 3.9+';

$plugin->outestssufficient = true;
